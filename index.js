(function() {
  'use strict';
  const format = require('util').format;
  const assign = require('object-assign');
 
  const defaults = {
    tenant: "DEFAULT",
    collectionName: "configuration",
    documentId: "default"
  };

  function middlewareWrapper(options) {
     // if options are static (either via defaults or custom options passed in), wrap in a function
    let optionsCallback = null;
    if (typeof options === 'function') {
      optionsCallback = options;
    }
    else {
      optionsCallback = function(req, cb) {
        cb(null, options);
      };
    }


    return function tenantMiddleware(req, res, next) {
      optionsCallback(req, function(err, options) {
        if (err) {
          next(err);
        }
        else {
          const o = assign({}, defaults, options);
          const { cache } = o;
          const cacheKey = (options.tenant || req.tenant || defaults.tenant) + '_' + o.collectionName;
          req.track('cacheKey: '+cacheKey)
          cache.get(cacheKey, (err, response) => {
            if (err) {next(err)
                req.track('Error reading from cache')
                next();
            };
            if (process.env.REDIS_CACHE_ENABLED && typeof response == 'object' && response != null) {
              req.track('saving configuration to req.'+o.collectionName);
              req[o.collectionName] = JSON.parse(response);
              next();
            }
            else {
              req.track('Configuration not found in cache, retreiving from db collection: '+o.collectionName+' with documentId: '+o.documentId);
              req.db.get(o.collectionName)
                .find({ id: o.documentId })
                .then(function(response) {
                  if (response) {
                    req.track('Document found');
                    let value= {};
                    value[o.documentId]= response[0];
                    req[o.collectionName]= value;
                    if (process.env.REDIS_CACHE_ENABLED) {
                      req.track('Saving document to cache');
                      cache.set(cacheKey, JSON.stringify(req[o.collectionName]), 'EX', process.env.REDIS_CACHE_EXPIRATION, (e)=>{
                        if (e) {
                            req.track('Error saving document to cache:'+JSON.stringify(e));
                        } else {
                            req.track('Saving to cache ok:');
                        }
                        next(e);
                      });
                    }
                    else {
                      req.track('Skipping save to cache');
                      next();
                    }
                  }
                  else {
                    const message=format('Not found: %s', format('Tenant configuration with key: %s not found', req.tenant));
                    req.track(message);
                    res.status(404).send({
                      success: false,
                      message: message
                    }).end();

                  }
                })
                .catch((err) => {
                  const message = format('Internal server error: %s', err);
                  req.track(message);
                  res.status(500).send({
                    success: false,
                    message: message
                  }).end();
                });
            }
          });
        }
      });
    };
  }
  module.exports = middlewareWrapper;
}());
